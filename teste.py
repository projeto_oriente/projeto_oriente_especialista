from oriente_especialista.similaridade import list_similaridade
from oriente_especialista.peewee_models import Documento


queryset_documentos = Documento.select().where(
    Documento.unsuported_file == False,
    Documento.downloaded == True,
    Documento.converted == True,
).order_by(Documento.index_local.desc())


print("list_similaridade")
list_similaridade(max_threads=32, documentos=queryset_documentos)
