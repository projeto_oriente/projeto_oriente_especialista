import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

install_dependencies = [
    'requests==2.21.0',
    'gensim==3.5.0',
    'python-decouple==3.1',
    'peewee==3.7.1',
    'filemagic==1.6',
]

setuptools.setup(
    name="oriente_especialista",
    version="0.0.3",
    author="Paulo Roberto Cruz",
    author_email="paulo.cruz9@gmail.com",
    description="Programa especialista de recomendações baseado em LSA para o sistema Oriente Web",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/projeto_oriente/projeto_oriente_especialista",
    packages=setuptools.find_packages(),
    install_requires=install_dependencies,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # include_package_data=True,
    package_data={
        '': ['*.txt'],
    },
)
