from oriente_especialista.config import timedelta, Timed
from oriente_especialista import run_especialista
from oriente_especialista.converte_txt import update_local_index
from oriente_especialista.corpus import create_corpus
from oriente_especialista.tfidf import create_tfidf_model
from oriente_especialista.lsi import create_lsi_model

from oriente_especialista.similaridade import update_lsi_index,list_similaridade
from oriente_especialista.peewee_models import Documento
import os

@timedelta
def upindex():
    queryset_documentos = Documento.select().where(
        Documento.unsuported_file == False,
        Documento.downloaded == True,
        Documento.converted == True,
    )
    print(queryset_documentos.count())

    for index, doc in enumerate(queryset_documentos):
        if index % 100 == 0:
            print(index)
        doc.index_local = index
        doc.save()

#update_local_index()
#create_corpus()
#create_tfidf_model()
#create_lsi_model()
#update_lsi_index()

queryset_documentos = Documento.select().where(
    Documento.unsuported_file == False,
    Documento.downloaded == True,
    Documento.converted == True,
    Documento.url_arquivo.contains('sigmauft.pythonanywhere'),
)

print("list_similaridade")
list_similaridade(max_threads=32)
