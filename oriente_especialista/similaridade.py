import os
from queue import Queue
from threading import Thread

from gensim.corpora import MmCorpus
from gensim.models import LsiModel
from gensim.similarities import Similarity
from gensim.test.utils import get_tmpfile
from oriente_libapi import Documento, Recomendacao

from oriente_especialista.config import API_MANAGER, timedelta
from oriente_especialista.config import CORPUS_PATH, LSI_MODEL_PATH, LSI_SIMILARIDADE_PATH
from oriente_especialista.peewee_models import Documento as DocumentoLocal

Documento.api_manager = API_MANAGER
Recomendacao.api_manager = API_MANAGER


@timedelta
def update_lsi_index():
    if os.path.isfile(LSI_SIMILARIDADE_PATH):
        os.remove(LSI_SIMILARIDADE_PATH)
    corpus = MmCorpus(CORPUS_PATH)
    lsi_model = LsiModel.load(LSI_MODEL_PATH)
    lsi_corpus = lsi_model[corpus]
    index_tmpfile = get_tmpfile("index")
    index = Similarity(index_tmpfile, lsi_corpus, num_features=300, num_best=10)
    index.save(LSI_SIMILARIDADE_PATH)


def list_similaridade_worker(i: int, q: Queue, s: Similarity):
    while True:
        documento = q.get()
        
        index_documento = s.similarity_by_id(documento.index_local)
        for index, documento_similar in enumerate(index_documento):
            print(documento.index_local, index, documento_similar)
            documento_similar_local = DocumentoLocal.get(DocumentoLocal.index_local == documento_similar[0])

            local_data = {
                "documento_origem": documento.id_api,
                "documento_destino": documento_similar_local.id_api,
                "fator": documento_similar[1],
            }
            recomendacao = Recomendacao(data=local_data)
            try:
                recomendacao.save()
            except Exception as e:
                print(e)
        
        q.task_done()


@timedelta
def list_similaridade(max_threads=16, documentos=None):
    q = Queue()
    lsa_index: Similarity = Similarity.load(LSI_SIMILARIDADE_PATH)

    for i in range(max_threads):
        t = Thread(
            target=list_similaridade_worker,
            args=(i, q, lsa_index)
        )
        t.setDaemon(True)
        t.start()
    
    if documentos is None:
        documentos = DocumentoLocal.select().where(
            DocumentoLocal.unsuported_file == False,
            DocumentoLocal.downloaded == True,
            DocumentoLocal.converted == True,
        )
    print(documentos.count())
    for documento in documentos:
        q.put(documento)
    print("esperando join")
    q.join()


@timedelta
def similaridade_lsi(max_threads=16):
    update_lsi_index()
    list_similaridade(max_threads)


if __name__ == "__main__":
    similaridade_lsi()
