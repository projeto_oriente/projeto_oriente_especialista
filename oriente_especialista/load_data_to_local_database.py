import os
from queue import Queue
from threading import Thread
from uuid import uuid4

from oriente_libapi import Documento

from oriente_especialista.config import API_MANAGER, timedelta
from oriente_especialista.config import PDFS_DIR, TXT_DIR, debug_print
from oriente_especialista.peewee_models import Documento as DocumentoDBLocal
from oriente_especialista.peewee_models import database_writer_thread

Documento.api_manager = API_MANAGER


def process_documento(database_q, index, documento: Documento):
    doc_id = documento.key("id")
    url_arquivo = documento.key("url_arquivo")

    doc_uuid = uuid4()
    original_filename = f"{doc_id}_{doc_uuid}"
    txt_filename = f"{doc_id}_{doc_uuid}.txt"

    original_filepath = os.path.join(PDFS_DIR, original_filename)
    txt_filepath = os.path.join(TXT_DIR, txt_filename)

    def action():
        DocumentoDBLocal.create_or_update(
            id_api=doc_id,
            index_local=index,
            url_arquivo=url_arquivo,
            arquivo_local_original=original_filepath,
            arquivo_local_txt=txt_filepath,
        )

    database_q.put(action)


def load_documento_worker(index: int, q: Queue, database_q: Queue):
    debug_print(f"Thread Worker Iniciada: {index}")
    while True:
        q_output = q.get()
        process_documento(database_q, *q_output)
        q.task_done()


@timedelta
def load_data_to_local_database(max_threads=10):
    q = Queue()
    database_queue = Queue()
    thread_list = []

    for i in range(max_threads):
        t = Thread(
            target=load_documento_worker,
            args=(i, q, database_queue)
        )
        t.setDaemon(True)
        t.start()
        thread_list.append(t)

    database_thread = Thread(
        target=database_writer_thread,
        args=(database_queue,),
    )
    database_thread.setDaemon(True)
    database_thread.start()

    for index, documento in enumerate(Documento.filter(sync="False").get_all()):
        if documento is not None:
            q.put((index, documento))

    q.join()
    database_queue.join()


if __name__ == "__main__":
    load_data_to_local_database(12)
