import os
from queue import Queue
from threading import Thread

from gensim.corpora import Dictionary
from gensim.utils import tokenize

from oriente_especialista.config import DICIONARIO_PATH, UNFILTERED_DICIONARIO_PATH, stoplist, pontuacao, timedelta
from oriente_especialista.peewee_models import Documento as DocumentoDBLocal


def get_text_as_list_from_txt(filename):
    textos = []

    with open(filename) as arquivo_handle:
        for linha in arquivo_handle.readlines():
            for p in pontuacao:
                linha = linha.replace(p, "")
            palavras = tokenize(linha, lowercase=True, deacc=True)
            textos += [p for p in palavras if p not in stoplist]

    return textos


def add_to_dictionary_worker(index: int, q: Queue, d_q: Queue,):
    print(f"Thread: {index} iniciada")

    while True:
        documento = q.get()
        print(documento.index_local)
        texto = get_text_as_list_from_txt(documento.arquivo_local_txt)
        d_q.put(texto)
        q.task_done()


def add_dict_worker(d:Dictionary, d_q:Queue):
    while True:
        print("add_dict")
        texto = d_q.get()
        d.add_documents([texto, ])
        d_q.task_done()


@timedelta
def create_dictionary(max_threads=16):
    if os.path.isfile(UNFILTERED_DICIONARIO_PATH):
        dictionary = Dictionary.load(UNFILTERED_DICIONARIO_PATH)
    else:
        dictionary = Dictionary()
    
    q = Queue()
    d_q = Queue()

    for i in range(max_threads):
        t = Thread(
            target=add_to_dictionary_worker,
            args=(i, q, d_q)
        )
        t.setDaemon(True)
        t.start()

    documentos = DocumentoDBLocal.select().where(
        DocumentoDBLocal.converted == True,
        DocumentoDBLocal.unsuported_file == False,
    )

    d_q_thread = Thread(
        target=add_dict_worker,
        args=(dictionary, d_q)
    )
    d_q_thread.setDaemon(True)
    d_q_thread.start()
    
    for documento in documentos:
        q.put(documento)

    q.join()
    d_q.join()

    dictionary.save(UNFILTERED_DICIONARIO_PATH)


@timedelta
def clean_dictionary():
    dicionario: Dictionary = Dictionary.load(UNFILTERED_DICIONARIO_PATH)
    dicionario.filter_extremes(no_below=10, no_above=0.5)
    dicionario.compactify()
    dicionario.save(DICIONARIO_PATH)


if __name__ == '__main__':
    create_dictionary()
    clean_dictionary()
