from oriente_especialista.peewee_models import init_models
from oriente_especialista.converte_txt import converte_documentos_txt
from oriente_especialista.corpus import create_corpus
from oriente_especialista.dictionary import create_dictionary, clean_dictionary
from oriente_especialista.download import download_documentos
from oriente_especialista.load_data_to_local_database import load_data_to_local_database
from oriente_especialista.lsi import create_lsi_model
from oriente_especialista.similaridade import similaridade_lsi
from oriente_especialista.tfidf import create_tfidf_model


def setup():
    init_models()

def run_especialista(max_threads:int=12):
    """
    run_especialista: executa processo completo para recomendações
    """
    load_data_to_local_database(max_threads)
    download_documentos(max_threads)
    converte_documentos_txt()
    create_dictionary()
    clean_dictionary()
    create_corpus()
    create_tfidf_model()
    create_lsi_model()
    similaridade_lsi()
