import os
import pickle
from queue import Queue
from threading import Thread
from gensim.corpora import Dictionary, MmCorpus
from gensim.utils import tokenize

from oriente_especialista.config import CORPUS_PATH, DICIONARIO_PATH, timedelta, Timed, TOKENDOCS_DIR
from oriente_especialista.peewee_models import Documento as DocumentoDBLocal

def get_tokens(filename, dictionary):
    with open(filename) as arquivo_handle:        
        texto = list()
        for linha in arquivo_handle.readlines():
            texto += tokenize(linha, lowercase=True, deacc=True)
        
        return dictionary.doc2bow(texto)

def tokenfilename(id):
    return os.path.join(TOKENDOCS_DIR, str(id))

def serialize_documents_worker(t_index:int, q:Queue, dic:Dictionary):
    print(f"Thread iniciada: {t_index}")
    while True:
        doc = q.get()
        filename = os.path.join(TOKENDOCS_DIR, str(doc.id_api))
        l = get_tokens(doc.arquivo_local_txt, dic)
        
        with open(filename, "wb") as fp:
            pickle.dump(l, fp)

        q.task_done()

@timedelta
def serialize_documents(max_threads = 16):
    doc_queue = Queue()
    dic:Dictionary = Dictionary.load(DICIONARIO_PATH)

    for i in range(max_threads):
        t = Thread(
            target=serialize_documents_worker,
            args=(i, doc_queue, dic),
        )
        t.setDaemon(True)
        t.start()

    documentos = DocumentoDBLocal.select()
    for doc in documentos:
        doc_queue.put(doc)
    
    print("esperando join")
    doc_queue.join()
    
    print("serialize docs")

class CorpusObject(object):

    def __iter__(self):
        documentos = DocumentoDBLocal.select().where(
            DocumentoDBLocal.converted == True,
            DocumentoDBLocal.unsuported_file == False
        )

        for documento in documentos:
            filename = tokenfilename(documento.id_api)            
            with open(filename, "rb") as fp:
                b = pickle.load(fp)
                yield b


@timedelta
def create_corpus():
    corpus_object = [documento for documento in CorpusObject()]

    if os.path.isfile(CORPUS_PATH):
        os.remove(CORPUS_PATH)
    MmCorpus.serialize(CORPUS_PATH, corpus_object)


if __name__ == "__main__":
    create_corpus()
