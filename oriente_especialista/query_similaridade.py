from oriente_especialista.config import timedelta
from oriente_especialista.config import LSI_SIMILARIDADE_PATH
from gensim.similarities import Similarity
from oriente_especialista.peewee_models import Documento

@timedelta
def query_similaridade():
    index = Similarity.load(LSI_SIMILARIDADE_PATH)
    documentos = Documento.select().where(Documento.url_arquivo.contains("sigmauft"))
    for doc in documentos:
        print(doc)


if __name__ == '__main__':
    query_similaridade()
