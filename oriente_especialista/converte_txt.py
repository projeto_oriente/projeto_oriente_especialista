import os
from queue import Queue
from subprocess import call
from threading import Thread

from oriente_especialista.config import debug_print, timedelta
from oriente_especialista.peewee_models import Documento as DocumentoDBLocal
from oriente_especialista.peewee_models import database_writer_thread


def converte_documentos_txt_worker(index, q: Queue, database_queue: Queue):
    while True:
        documento = q.get()

        debug_print(documento.arquivo_local_original)

        if not documento.converted or not os.path.isfile(documento.arquivo_local_txt):

            if os.path.isfile(documento.arquivo_local_original):
                target_path_txt = documento.arquivo_local_txt
                debug_print("Conversão para txt")

                if os.path.isfile(target_path_txt):
                    documento.converted = True
                else:
                    debug_print("Arquivo:", target_path_txt)

                    call(["pdftotext", "-enc", "UTF-8", documento.arquivo_local_original, target_path_txt])
                    if os.path.isfile(target_path_txt):
                        documento.converted = True
                    else:
                        documento.converted = False
                        documento.unsuported_file = True
                        debug_print("Formato não suportado no momento")

            def action():
                documento.save()

            database_queue.put(action)

        q.task_done()


def mudar_index_local_worker(index, q: Queue, database_queue: Queue):
    while True:
        index_local, documento = q.get()
        documento.index_local = index_local

        def action():
            documento.save()

        database_queue.put(action)
        q.task_done()


@timedelta
def converte_documentos_txt(max_threads=12):
    q = Queue()
    database_queue = Queue()

    database_thread = Thread(
        target=database_writer_thread,
        args=(database_queue,),
    )
    database_thread.setDaemon(True)
    database_thread.start()

    for i in range(max_threads):
        t = Thread(
            target=converte_documentos_txt_worker,
            args=(i, q, database_queue)
        )
        t.setDaemon(True)
        t.start()

    documentos = DocumentoDBLocal.select().where(
        DocumentoDBLocal.downloaded == True,
        DocumentoDBLocal.converted == False,
        DocumentoDBLocal.unsuported_file == False
    )

    for documento in documentos:
        q.put(documento)
    q.join()

    index_queue = Queue()
    # atualiza indexes dos documentos

    for i in range(max_threads):
        t = Thread(
            target=mudar_index_local_worker,
            args=(i, index_queue, database_queue)
        )
        t.setDaemon(True)
        t.start()

    total_documentos = DocumentoDBLocal.select().count() + 10
    documentos_fora = DocumentoDBLocal.select().where(DocumentoDBLocal.converted == False)
    for index, documento in enumerate(documentos_fora):
        # muda index dos documentos que não entram na recomendação
        index_queue.put((index + total_documentos, documento))

    documentos_dentro = DocumentoDBLocal.select().where(DocumentoDBLocal.converted == True)
    for index, documento in enumerate(documentos_dentro):
        # muda index dos documentos que entram na recomendação
        index_queue.put((index, documento))

    index_queue.join()
    database_queue.join()

@timedelta
def update_local_index(max_threads=12):
    database_queue = Queue()
    index_queue = Queue()
    
    database_thread = Thread(
        target=database_writer_thread,
        args=(database_queue,),
    )
    database_thread.setDaemon(True)
    database_thread.start()

    for i in range(max_threads):
        t = Thread(
            target=mudar_index_local_worker,
            args=(i, index_queue, database_queue)
        )
        t.setDaemon(True)
        t.start()

    total_documentos = DocumentoDBLocal.select().count() + 10
    documentos_fora = DocumentoDBLocal.select().where(DocumentoDBLocal.converted == False)
    for index, documento in enumerate(documentos_fora):
        # muda index dos documentos que não entram na recomendação
        index_queue.put((index + total_documentos, documento))

    documentos_dentro = DocumentoDBLocal.select().where(DocumentoDBLocal.converted == True)
    for index, documento in enumerate(documentos_dentro):
        # muda index dos documentos que entram na recomendação
        index_queue.put((index, documento))

    index_queue.join()
    database_queue.join()


if __name__ == "__main__":
    converte_documentos_txt()
