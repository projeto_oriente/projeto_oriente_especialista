from queue import Queue

import peewee
from oriente_especialista.config import DATABASE_PATH

banco = peewee.SqliteDatabase(DATABASE_PATH)


class Documento(peewee.Model):
    id_api = peewee.IntegerField(unique=True)
    index_local = peewee.IntegerField()
    url_arquivo = peewee.CharField(unique=True)
    arquivo_local_original = peewee.CharField(unique=True)
    arquivo_local_txt = peewee.CharField(unique=True)
    unsuported_file = peewee.BooleanField(default=False)
    downloaded = peewee.BooleanField(default=False)
    converted = peewee.BooleanField(default=False)

    class Meta:
        database = banco
        order_by = ('index_local',)

    def __str__(self):
        return f"{self.id}: {self.url_arquivo}: {self.downloaded}: {self.converted}"

    @classmethod
    def create_or_update(cls, **kwargs):
        id_api = kwargs.get('id_api')
        with banco.atomic() as txn:
            try:
                Documento.get(Documento.id_api == id_api)
                cls.update(**kwargs).where(Documento.id_api == id_api)
            except peewee.DoesNotExist:
                cls.create(**kwargs)


def database_writer_thread(database_queue: Queue):
    """
    Escrita no banco de dados acontece somente nesta thread
    """
    while True:
        action = database_queue.get()
        action()
        database_queue.task_done()


def init_models():
    try:
        Documento.create_table()
    except peewee.OperationalError:
        print('Tabela Documento ja existe!')


if __name__ == "__main__":
    init_models()
