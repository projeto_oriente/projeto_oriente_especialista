from gensim.corpora import MmCorpus
from gensim.models import TfidfModel
import os
from oriente_especialista.config import TFIDF_MODEL_PATH, CORPUS_PATH, timedelta


@timedelta
def create_tfidf_model():
    corpus_object = MmCorpus(CORPUS_PATH)
    tfidf_model = TfidfModel(corpus_object)
    if os.path.isfile(TFIDF_MODEL_PATH):
        os.remove(TFIDF_MODEL_PATH)
    tfidf_model.save(TFIDF_MODEL_PATH)


if __name__ == '__main__':
    create_tfidf_model()
