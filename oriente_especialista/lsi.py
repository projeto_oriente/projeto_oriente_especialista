from gensim.corpora import MmCorpus, Dictionary
from gensim.models import LsiModel, TfidfModel
import os
from oriente_especialista.config import DICIONARIO_PATH, TFIDF_MODEL_PATH, CORPUS_PATH, LSI_MODEL_PATH, timedelta


@timedelta
def create_lsi_model():
    corpus_object = MmCorpus(CORPUS_PATH)
    dicionario = Dictionary.load(DICIONARIO_PATH)
    tfidf_model = TfidfModel.load(TFIDF_MODEL_PATH)
    corpus_tfidf = tfidf_model[corpus_object]
    modelo_lsi = LsiModel(corpus_tfidf, id2word=dicionario, num_topics=300)
    if os.path.isfile(LSI_MODEL_PATH):
        os.remove(LSI_MODEL_PATH)
    modelo_lsi.save(LSI_MODEL_PATH)


if __name__ == '__main__':
    create_lsi_model()
