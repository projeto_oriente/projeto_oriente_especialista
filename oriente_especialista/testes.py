import magic
from gensim import corpora
from gensim import models


def teste_1():
    corpus = corpora.MmCorpus("files/corpora/corpus.mm")
    dictionary = corpora.Dictionary.load('files/corpora/corpus.dict')

    # lsi = models.LsiModel(corpus, id2word=dictionary)
    tfidf = models.TfidfModel(corpus)

    doc = corpus[0]
    doc_vec = tfidf[doc]
    print(doc_vec)


def teste_2():
    with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
        print(m.id_filename('files/pdfs/1.pdf'))
