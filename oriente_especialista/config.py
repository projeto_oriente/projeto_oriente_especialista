import os
import time

import magic
import urllib3
from decouple import config
from oriente_libapi import ApiManager

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

API_MANAGER = ApiManager(
    user=config("oriente_user", default="admin"),
    password=config("oriente_password", default="1234567o"),
    endpoint=config("oriente_endpoint", default="http://127.0.0.1:8000/api"),
)

FORMATOS_ACEITOS = {"application/pdf": "pdf", }

LOCAL_DIR = os.path.dirname(__file__)
BASE_DIR = config("BASE_DIR", default=LOCAL_DIR)

FILES_DIR = os.path.join(BASE_DIR, "files")
PDFS_DIR = os.path.join(FILES_DIR, "pdfs")
TXT_DIR = os.path.join(FILES_DIR, "txts")
TOKENDOCS_DIR = os.path.join(FILES_DIR, "tokendocs")
CORPORA_DIR = os.path.join(FILES_DIR, "corpora")

LISTA_DOCUMENTO_PATH = os.path.join(FILES_DIR, "lista_documentos.csv")

UNFILTERED_DICIONARIO_PATH = os.path.join(CORPORA_DIR, "unfiltered_dicionario.dict")
DICIONARIO_PATH = os.path.join(CORPORA_DIR, "dicionario.dict")

CORPUS_PATH = os.path.join(CORPORA_DIR, "corpus.mm")
TFIDF_MODEL_PATH = os.path.join(CORPORA_DIR, "modelo.tfidf")
LSI_MODEL_PATH = os.path.join(CORPORA_DIR, "modelo.lsi")
LSI_SIMILARIDADE_PATH = os.path.join(CORPORA_DIR, "similaridade.index")

stoplist = [palavra.strip() for palavra in open(os.path.join(LOCAL_DIR, "stopwords.txt")).readlines()]

pontuacao = [palavra.strip() for palavra in open(os.path.join(LOCAL_DIR, "ignorar.txt")).readlines()]

DEBUG = config("DEBUG", cast=bool, default=False)

DATABASE_PATH = os.path.join(FILES_DIR, 'banco.db')

def debug_print(*args, **kwargs):
    if DEBUG:
        print(*args, **kwargs)


# criar pastas se não existirem

if not os.path.exists(FILES_DIR):
    os.makedirs(FILES_DIR)

if not os.path.exists(PDFS_DIR):
    os.makedirs(PDFS_DIR)

if not os.path.exists(TXT_DIR):
    os.makedirs(TXT_DIR)

if not os.path.exists(CORPORA_DIR):
    os.makedirs(CORPORA_DIR)

if not os.path.exists(TOKENDOCS_DIR):
    os.makedirs(TOKENDOCS_DIR)


def timedelta(callable):
    def r(*args, **kwargs):
        print(f"{callable.__name__}: START")
        start_time = time.time()
        callable(*args, **kwargs)
        end_time = time.time()
        print(f"{callable.__name__}: END - {end_time - start_time}")

    return r


class Timed(object):

    def __enter__(self):
        self.start_time = time.time()
        return self.start_time

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end_time = time.time()
        print(f"time spend {self.end_time - self.start_time}")


def is_file_suported(filename):
    file_mimetype = None

    with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
        file_mimetype = m.id_filename(filename)

    if file_mimetype is None:
        return False
    else:
        return file_mimetype in FORMATOS_ACEITOS


def setup():
    pass
