import os
from queue import Queue
from threading import Thread

import requests

from oriente_especialista.config import debug_print, timedelta, is_file_suported
from oriente_especialista.peewee_models import Documento as DocumentoDBLocal
from oriente_especialista.peewee_models import database_writer_thread


def process_download(documento: DocumentoDBLocal, database_q: Queue):
    print(f"download {documento.url_arquivo}")
    if os.path.isfile(documento.arquivo_local_original):
        documento.downloaded = True

    if not documento.downloaded:
        with requests.Session() as session:
            debug_print("Arquivo:", documento.arquivo_local_original)
            response = session.get(documento.url_arquivo, stream=True, verify=False)
            handle = open(documento.arquivo_local_original, "wb")
            for chunk in response.iter_content(chunk_size=512):
                if chunk:
                    handle.write(chunk)
            debug_print("Download Concluido")

    if os.path.isfile(documento.arquivo_local_original):
        documento.downloaded = True
        if is_file_suported(documento.arquivo_local_original):
            documento.unsuported_file = False

    else:
        debug_print("Download não executado!")

    def action():
        documento.save()

    database_q.put(action)


def download_worker(index: int, q: Queue, database_q: Queue):
    debug_print(f"Thread Worker Iniciada: {index}")
    while True:
        documento = q.get()
        process_download(documento, database_q)
        q.task_done()


@timedelta
def download_documentos(max_threads=10):
    q = Queue()
    database_queue = Queue()
    thread_list = []

    for i in range(max_threads):
        t = Thread(
            target=download_worker,
            args=(i, q, database_queue)
        )
        t.setDaemon(True)
        t.start()
        thread_list.append(t)

    database_thread = Thread(
        target=database_writer_thread,
        args=(database_queue,),
    )
    database_thread.setDaemon(True)
    database_thread.start()

    documentos = DocumentoDBLocal.select().where(DocumentoDBLocal.downloaded == False)
    for documento in documentos:
        q.put(documento)

    q.join()
    print("q.join")
    database_queue.join()
    print("database_queue")


@timedelta
def download_check():
    '''
    documentos = DocumentoDBLocal.select().where(DocumentoDBLocal.downloaded == True)
    print(f"Documentos baixados: {documentos.count()}")
    for documento in documentos:
        if not is_file_suported(documento.arquivo_local_original):
            print(documento)
    '''
    documentos = DocumentoDBLocal.select().where(DocumentoDBLocal.downloaded == False)

    print(f"Documentos não baixados ainda: {documentos.count()}")
    for documento in documentos:
        print(documento)


if __name__ == "__main__":
    download_documentos(16)
    # download_check()
    pass
