import os
from queue import Queue
from threading import Thread

from oriente_especialista.config import timedelta
from oriente_especialista.peewee_models import Documento


def verificar_conversao_worker(index: int, q: Queue, database_q: Queue):
    while True:
        documento = q.get()
        if not os.path.isfile(documento.arquivo_local_txt):
            database_q.put(documento)

        q.task_done()


def database_writer_thread(database_q: Queue):
    while True:
        documento = database_q.get()
        print("*" * 30)
        print("salvando documento: ", documento.id_api)
        print("documento não convertido")
        print(documento.arquivo_local_txt)
        documento.converted = False
        documento.unsuported_file = True
        documento.save()
        print(documento)
        database_q.task_done()


@timedelta
def verificar_conversao(max_threads=16):
    documentos_queue = Queue()
    database_queue = Queue()

    database_thread = Thread(
        target=database_writer_thread,
        args=(database_queue,)
    )
    database_thread.setDaemon(True)
    database_thread.start()

    for i in range(max_threads):
        t = Thread(
            target=verificar_conversao_worker,
            args=(i, documentos_queue, database_queue)
        )
        t.setDaemon(True)
        t.start()

    # atualiza todos os documentos baixados não marcados como unsuported_file
    Documento.update(converted=True).where(
        Documento.downloaded == True,
        Documento.unsuported_file == False
    ).execute()

    documentos_queryset = Documento.select().where(
        Documento.unsuported_file == False,
    )
    print(f"Total de Documentos: {documentos_queryset.count()}")

    for documento in documentos_queryset:
        documentos_queue.put(documento)

    print("esperando documentos_queue")
    documentos_queue.join()

    print("esperando database_queue")
    database_queue.join()

    documentos_convertidos = Documento.select().where(
        Documento.converted == True
    )

    print("Total Documentos Convertidos: ", documentos_convertidos.count())
    for index, documento in enumerate(documentos_convertidos):
        documento.index_local = index
        documento.save()


if __name__ == '__main__':
    verificar_conversao()
